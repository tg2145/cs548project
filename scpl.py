
from queue import Queue

'''
Calculates the shortest path on graph from node u to node v
n is the total verticies in the graph
'''
def distance(graph, u, v, n):
	visited = [False] * n
	distance = [0] * n

	Q = Queue()
	distance[u] = 0

	Q.put(u)
	visited[u] = True
	while (not Q.empty()):
		x = Q.get()
		
		for i in range(len(graph[x])):
			if (visited[graph[x][i]]):
				continue

			distance[graph[x][i]] = distance[x] + 1
			Q.put(graph[x][i])
			visited[graph[x][i]] = True
	return distance[v]

'''
Created an undirected edge between node u and v in graph
'''
def addEdge(graph, u, v):
	graph[u].append(v)
	graph[v].append(u)


'''
S must be such that S(1) = 1
and S(a) >= S(b) when a < b

Here are three fuzzy definitions of "close" based on that description
'''
def S1(x):
    return min(1,max(0, 1-((x-2)/4)))

def S2(x):
    return min(1,max(0, 1-((x-1)/3)))

def S3(x):
    return 1/x

'''
returns the soft characteristic path length of a graph
described in section 2.2
'''
def SL(graph, definition, totalVerticies):
    if definition == 1:
        S = S1
    elif definition == 2:
        S = S2
    else:
        S = S3
    norm = 1/(totalVerticies*(totalVerticies-1))
    total = 0
    for i in range(totalVerticies):
        for j in range(totalVerticies):
            if i != j: total += S(distance(graph, i, j, totalVerticies))
    return total*norm

'''
Fully connected graphs should have closeness of 1 as per the contraints
'''
def testCase1():
    t1 = 3
    g1 = [[] for i in range(t1)]
    addEdge(g1, 0, 1)
    addEdge(g1, 0, 2)
    addEdge(g1, 1, 2)
    assert SL(g1, 1, t1) == 1
    assert SL(g1, 2, t1) == 1
    assert SL(g1, 3, t1) == 1

    t2 = 8
    g2 = [[] for i in range(t2)]
    addEdge(g2, 0, 1); addEdge(g2, 0, 2); addEdge(g2, 0, 3); addEdge(g2, 0, 4); addEdge(g2, 0, 5); addEdge(g2, 0, 6); addEdge(g2, 0, 7)
    addEdge(g2, 1, 2); addEdge(g2, 1, 3); addEdge(g2, 1, 4); addEdge(g2, 1, 5); addEdge(g2, 1, 6); addEdge(g2, 1, 7)
    addEdge(g2, 2, 3); addEdge(g2, 2, 4); addEdge(g2, 2, 5); addEdge(g2, 2, 6); addEdge(g2, 2, 7)
    addEdge(g2, 3, 4); addEdge(g2, 3, 5); addEdge(g2, 3, 6); addEdge(g2, 3, 7)
    addEdge(g2, 4, 5); addEdge(g2, 4, 6); addEdge(g2, 4, 7)
    addEdge(g2, 5, 6); addEdge(g2, 5, 7)
    addEdge(g2, 6, 7)
    assert SL(g2, 1, t2) == 1
    assert SL(g2, 2, t2) == 1
    assert SL(g2, 3, t2) == 1
    print("Test Case 1 Passed")

'''
Test case where values were manually calculated beforehand
'''
def testCase2():
    t = 4
    g = [[] for i in range(t)]
    addEdge(g, 0, 1)
    addEdge(g, 1, 2)
    addEdge(g, 2, 3)
    assert abs(SL(g, 1, t) - .958333) < .0001
    assert abs(SL(g, 2, t) - .777777) < .0001
    assert abs(SL(g, 3, t) - .722222) < .0001
    print("Test Case 2 Passed")

if __name__ == '__main__':

    print("Running Test Cases")
    testCase1()
    testCase2()
    
    totalVerticies = 9
    graph = [[] for i in range(totalVerticies)]
    addEdge(graph, 0, 1)
    addEdge(graph, 0, 7)
    addEdge(graph, 1, 7)
    addEdge(graph, 1, 2)
    addEdge(graph, 2, 3)
    addEdge(graph, 2, 5)
    addEdge(graph, 2, 8)
    addEdge(graph, 3, 4)
    addEdge(graph, 3, 5)
    addEdge(graph, 4, 5)
    addEdge(graph, 5, 6)
    addEdge(graph, 6, 7)
    addEdge(graph, 7, 8)

    print("\nSoft Characteristic Path Lengths of the graph:")
    print("Using S1:")
    print(SL(graph, 1, totalVerticies))
    print("Using S2:")
    print(SL(graph, 2, totalVerticies))
    print("Using S3:")
    print(SL(graph, 3, totalVerticies))

